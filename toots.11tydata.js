const dataScripts = require("./src/datascripts");

module.exports = {
  layout: "default",
  toots: dataScripts.filterOutboxToots(),
  pagination: {
    data: "toots",
    alias: "post",
    size: 200,
    reverse: true
  },
  permalink: '{% if pagination.pageNumber > 0 %}/page/{{ pagination.pageNumber }}/index.html{% endif %}{% if pagination.pageNumber == 0 %}/toots.html{% endif %}'
};