const dataScripts = require("./src/datascripts");

module.exports = {
  layout: "default",
  toots: dataScripts.filterOutboxToots(),
  pagination: {
    data: "toots",
    size: 1,
    alias: "post",
    addAllPagesToCollections: true,
  },
  permalink: ({post}) => "/toot/"+dataScripts.extractStatusID(post.id)+"/",
  eleventyComputed: {
    id: ({post}) => dataScripts.extractStatusID(post.id),
    content: ({post}) => dataScripts.extractObjectContent(post),
    date: ({post}) => post.published,
    url: ({post}) => post.object.url
  },
};