const outbox = require("../_data/outbox.json");

class datascripts {

    extractStatusID(value) {
        const re = /(\/\d+\/)/;
        // extract /ID/ with regex
        const id = value.match(re);
        // remove forward slashes
        return id[1].replace(/[\/]+/g, "");
    }

    extractObjectContent(post) {
        if ( typeof post.object === "string" ) {
            return post.object;
        } else {
            return post.object.content;
        }
    }

    filterOutboxToots() {
        const toots = outbox.orderedItems.filter( (obj) => {
            if ( obj.to[0] === "https://www.w3.org/ns/activitystreams#Public") {
                return true;
            } else {
                return false;
            }
        } );
        return toots;
    }

}
module.exports = new datascripts();