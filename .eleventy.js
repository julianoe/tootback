const { execSync } = require('child_process');
const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");

module.exports = function(eleventyConfig) {
    
    eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
    
    eleventyConfig.addPassthroughCopy("assets");

    eleventyConfig.addFilter("dateToLocaleString", function(value) {
        const d = new Date(value);
        return d.toLocaleDateString()+ " • " + d.toLocaleTimeString();
    });


    eleventyConfig.addFilter("extractStatusID", function(value) {
        return extractStatusID(value);
    });

    function extractStatusID(value) {
        const re = /(\/\d+\/)/;
        // extract /ID/ with regex
        const id = value.match(re);
        // remove forward slashes
        return id[1].replace(/[\/]+/g, "");
    }

    // pagefind search plugin
	eleventyConfig.on('eleventy.after', () => {
		console.log('[pagefind] Creating search index.');
		execSync(`npx pagefind --site _site/toot --glob \"[0-9]*/**/*.html\"`, { encoding: 'utf-8' });
    });


};

